<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@home');
Route::post('/contactus', 'HomeController@contactus');
Route::get('/questions', 'AnswerController@questions');
Route::post('/questions', 'AnswerController@store');
Route::get('/thankyou', 'AnswerController@thank');
Route::get('/redirect', 'RedirectController@handle');
Route::get('/about', 'AboutController@about');
Route::get('/pricing', 'PricingController@show');
Route::get('/courses', 'CoursesController@show');


Route::group(['minddleware' => 'admin'], function () {

    Route::get('/admin/dashboard', 'Admin\DashboardController@index');
    Route::get('/admin/question', 'Admin\QuestionnaireController@index');
    Route::post('/admin/question', 'Admin\QuestionnaireController@store');
    Route::patch('/admin/question/{question}', 'Admin\QuestionnaireController@update');
    Route::delete('/admin/question/{question}', 'Admin\QuestionnaireController@destroy');
    Route::post('/admin/options', 'Admin\OptionsController@store');
    Route::get('/admin/options/{option}', 'Admin\OptionsController@show');
    Route::patch('/admin/options/{options}', 'Admin\OptionsController@update');
    Route::delete('/admin/options/{options}', 'Admin\OptionsController@destroy');
    Route::resource('/admin/user', 'Admin\UserController');
    Route::resource('/admin/form', 'Admin\SubmissionController');
    Route::resource('/admin/newform', 'Admin\SubmissionController@new');
    Route::resource('/admin/addnote', 'Admin\SubmissionController@note');
    Route::get('/admin/contactus', 'Admin\ContactusController@new');
    Route::get('/admin/contactall', 'Admin\ContactusController@index');
    Route::get('/admin/contactus/{id}', 'Admin\ContactusController@show');

});
