<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectController extends Controller
{
    public function handle()
    {
        if (!Auth::check())
        {
            return redirect('/login');
        }elseif (Auth::user()->is_admin == 1)
        {
            return redirect('/admin/dashboard');
        }
        else{
            return redirect('/');
        }
    }
}
