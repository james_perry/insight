<?php

namespace App\Http\Controllers;

use App\ContactForm;
use App\Contactus;
use App\Notifications\NewContactFormNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('home');
    }
    public function contactus(Request $request)
    {

        $contacdata = ContactForm::create([
            'firstname' => $request->ufirstname,
            'lastname' => $request->ulastname,
            'phone' => $request->umobile,
            'email' => $request->uemail,
            'message' => $request->message,
            'status' => '1'
        ]);

        $contacdata->save();

        $users = User::where('is_admin', '1')->get();
        foreach ($users as $user)
        {
            $user->notify(new NewContactFormNotification());
        }

        return Redirect::action('AnswerController@thank');
    }
}
