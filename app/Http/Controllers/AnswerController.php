<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Notifications\NewFormSubmittedNotification;
use App\Question;
use App\Questionnaire;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function questions()
    {
        $total = Question::count();
        $questions = Question::all();
        return View::make('questions', compact('questions', 'total'));
    }

    public function store(Request $request)
    {
       $user = $this->checkUser($request);
       $form = $this->saveForm($request, $user);
       $this->saveAnswers($request, $form);

        $users = User::where('is_admin', '1')->get();
        foreach ($users as $user)
        {
            $user->notify(new NewFormSubmittedNotification());
        }

       return Redirect::action('AnswerController@thank');
    }

    public function checkUser(Request $request)
    {
            $user = User::where('email', $request->uemail)->first();
            if (is_object($user))
            {
                if (Auth::guest()){
                    Auth::guard()->login($user);
                    return $user;
                }else{
                    return $user;
                }
            }else{
                $user =  User::create([
                    'first_name' => $request->ufirstname,
                    'last_name' => $request->ulastname,
                    'mobile' => $request->uphone,
                    'email' => $request->uemail,
                    'password' => bcrypt(str_random(20)),
                ]);

                Auth::guard()->login($user);
                return $user;
            }
    }

    public function saveForm(Request $request, User $user)
    {
        $question = Questionnaire::create([
            'user_id' => $user->id,
            'name' => $request->child_name,
            'message' => $request->message,
            'status' => '1',
        ]);

        return $question;
    }

    public function saveAnswers($request, $form)
    {
        foreach ($request->answer as $answer)
        {
            list($que, $opt) = explode("_", $answer);

            Answer::create([
                'questionnaire_id' => $form->id,
                'question_id' => $que,
                'option_id' => $opt,
            ]);
        }
    }

    public function thank()
    {
        return view('thankyou');
    }


}
