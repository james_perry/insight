<?php

namespace App\Http\Controllers;

use App\ContactForm;
use App\Contactus;
use App\Notifications\NewContactFormNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CoursesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('courses');
    }
}
