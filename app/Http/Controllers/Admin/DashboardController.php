<?php

namespace App\Http\Controllers\Admin;

use App\Questionnaire;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;


class DashboardController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $users = User::where('is_admin', '0')->orderBy('created_at', 'desc')->take(8)->get();
        $questions = Questionnaire::orderBy('created_at', 'desc')->take(7)->get();
        return View::make('admin.home', compact('users', 'questions'));

    }

}
