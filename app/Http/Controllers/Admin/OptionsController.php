<?php

namespace App\Http\Controllers\Admin;

use App\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class OptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request,[
            'question' => 'required|digits_between:1,10',
            'answer' => 'required|string',
            'prefrence' => 'required|digits_between:1,3'
        ]);

        Option::create([
            'question_id' => $request->question,
            'answer' => $request->answer,
            'position' => $request->prefrence
        ]);

        return Redirect::action('Admin\OptionsController@show', $request->question);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $options = Option::where('question_id', $id)->paginate(15);
        return View::make('admin.questions.options.list', compact('options', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'question' => 'required|digits_between:1,10',
            'answer' => 'required|string',
            'prefrence' => 'required|digits_between:1,3'
        ]);

        Option::where('id', $id)->update([
            'question_id' => $request->question,
            'answer' => $request->answer,
            'position' => $request->prefrence
        ]);

        return Redirect::action('Admin\OptionsController@show', $request->question);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Option::destroy($id);
        return Redirect::action('Admin\OptionsController@show', $request->question);

    }
}
