<?php

namespace App\Http\Controllers\Admin;

use App\Note;
use App\Question;
use App\Questionnaire;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class SubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = Questionnaire::paginate(15);
        return View::make('admin.forms.list', compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $question = Questionnaire::where('id', $id)->first();
        $update = Questionnaire::where('id', $id)->update(['status' => '2']);
        $quests = Question::all();
        return View::make('admin.forms.details', compact('question', 'quests'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function new()
    {
        $forms = Questionnaire::where('status', '1')->paginate(15);
        return View::make('admin.forms.new', compact('forms'));
    }

    public function note(Request $request)
    {
        $id = Auth::user()->id;

        Note::create([
            'user_id' => $id,
            'questionnaire_id' => $request->question,
            'note' => $request->note,
        ]);

        return Redirect::action('Admin\SubmissionController@show', $request->question);
    }
}
