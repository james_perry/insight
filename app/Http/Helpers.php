<?php

/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/


use Illuminate\Support\Facades\Request;

    function isActivePath($path, $output = "active")
    {

        if (Request::path() ==  $path) return $output;
    }

    /*
    |--------------------------------------------------------------------------
    | Detect Active Routes
    |--------------------------------------------------------------------------
    |
    | Compare given routes with current route and return output if they match.
    | Very useful for navigation, marking if the link is active.
    |
    */
    function areActivePaths(Array $paths, $output = "active")
    {

        foreach ($paths as $path)
        {
            //return $path;
            if (Request::path() ==  $path) return $output;
        }

    }

        function in_room($value, $objects) {

            if (is_object($objects)) {
                foreach($objects as $object) {
                    if ($object->room_id == $value) return true;
                }
            }

            return false;
        }

        function in_rateplan($value, $objects) {

            if (is_object($objects)) {
                foreach($objects as $object) {
                    if ($object->rate_plan_id == $value) return true;
                }
            }

            return false;
        }

        function logstatus($status)
        {
            if ($status == '0')
            {
                return 'status_inprogress';
            }
            elseif ($status == '1')
            {
                return 'status_inprogress';
            }
            elseif ($status == '2')
            {
                return 'status_ok';
            }
            elseif ($status == '3')
            {
                return 'status_error error';
            }
        }




