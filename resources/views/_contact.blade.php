<div class="row">
    <div class="col-sm-6">
        <input type="text" required placeholder="First Name" name="ufirstname" @if(Auth::guest()) value="{{ old('ufirstname') }}" @else value="{{ Auth::user()->first_name }}" @endif class="form-control">
    </div>
    <div class="col-sm-6">
        <input type="text" required placeholder="Last Name" name="ulastname"  @if(Auth::guest()) value="{{ old('ulastname') }}" @else value="{{ Auth::user()->last_name }}" @endif class="form-control">
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <input type="text" required placeholder="Your Phone"  name="umobile"  @if(Auth::guest()) value="{{ old('umobile') }}" @else value="{{ Auth::user()->mobile }}" @endif class="form-control">
    </div>

    <div class="col-sm-6">
        <input type="text" required placeholder="Your Email" name="uemail"  @if(Auth::guest()) value="{{ old('uemail') }}" @else value="{{ Auth::user()->email }}" @endif class="form-control">
    </div>
</div>
<textarea required placeholder="Your Message" rows="5" cols="5" class="form-control" id="textarea" name="message">{{ old('message') }}</textarea>
<input type="submit" class="btn btn-info" value="Submit">

