@extends('layouts.app')
@section('title')Insight Education Questions @endsection
@section('content')

    <div class="body_contents">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h4>Can tick more than one box for questions</h4>

                    <h2>Initial Learning Assessment- <strong>FREE</strong> <div class="clear15px visible-xs visible-sm visible-md"></div><a href="#" class="btn btn-primary btn-lg">Start the Analysis</a> </h2>

                    <div class="questionair_data">
                        <form action="/questions" method="post" name="questionare" id="questionare">
                            {{ csrf_field() }}

                            <div class="step-1">
                                <fieldset>
                                    <legend><strong> 1. Who is this for?</strong></legend>
                                    <div class="{{ $errors->has('child_name') ? ' has-error' : '' }}">
                                        <label for="child_name" class="cols-sm-2 control-label">Child to be Assesed</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                                <input id="child_name" type="text" placeholder="Enter your child Name" class="form-control" name="child_name" value="{{ old('child_name') }}" required autofocus>
                                            </div>
                                            @if ($errors->has('child_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('child_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </fieldset>
                                <button class="read_more_button" id="btn-1" type="button"><i class="fa fa-arrow-right"></i> Next</button>
                            </div>

                            @foreach($questions as $question)
                                <div class="step-{{$loop->index + 2}}">
                                    <fieldset>
                                        <legend><strong>{{$loop->index + 2}}. {{$question->question}}:</strong></legend>
                                        <ul class="checkbox_list">
                                            @foreach($question->options as $answer)
                                                @if($question->type == 1)
                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="answer[]" value="{{$question->id}}_{{$answer->id}}">
                                                                {{$answer->answer}}
                                                            </label>
                                                        </div>
                                                        @else
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="answer[]" value="{{$question->id}}_{{$answer->id}}">
                                                                    {{$answer->answer}}
                                                                </label>
                                                            </div>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                        <button class="read_more_button" id="btn-{{$loop->index + 2}}" type="button"><i class="fa fa-arrow-right"></i> Next</button>
                                    </fieldset>
                                </div>
                                @endforeach

                            <div class="step-{{$total+2}}">

                                <div class="form-group">

                                    <strong>{{$total+2}}. Enter your name and email address:</strong><br>
                                    Enter your name and email to get access to your results
                                    <div class="clear10px"></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="First Name" name="ufirstname" @if(Auth::guest()) value="{{ old('ufirstname') }}" @else value="{{ Auth::user()->first_name }}" @endif class="form-control">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="Last Name" name="ulastname"  @if(Auth::guest()) value="{{ old('ulastname') }}" @else value="{{ Auth::user()->last_name }}" @endif class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="Your Phone"  name="uphone"  @if(Auth::guest()) value="{{ old('uphone') }}" @else value="{{ Auth::user()->mobile }}" @endif class="form-control">
                                        </div>

                                        <div class="col-sm-6">
                                            <input type="text" placeholder="Your Email" name="uemail"  @if(Auth::guest()) value="{{ old('uemail') }}" @else value="{{ Auth::user()->email }}" @endif class="form-control">
                                        </div>
                                    </div>

                                </div>


                                <textarea placeholder="Your Message" rows="5" cols="5" class="form-control" id="textarea" name="message"  value="{{ old('yourmessage') }}"></textarea>

                                <button type="submit" id="btn-{{$total+2}}" class="read_more_button">Submit</button>
                            </div>


                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <img src="images/form_page_right_img1.png" alt="">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scriptfooter')
    <script type="text/javascript" src="{{ asset('/js/questioair.js') }}"></script>
@endsection

