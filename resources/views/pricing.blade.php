
@extends('layouts.app')
@section('modal')<div class="container">

  
</div>

</body>

@endsection
@section('title')
    Insight Education
    @endsection
@section('content')

    <section id="welcome_section">
        <div class="container">
            <div class="col-sm-8 col-sm-offset-2">

            <h2><strong>Pricing</strong></h2>
	    <table style="border: 1px solid black; width: 100%;">
		<tr style="border: 1px solid black;">
		    <th>Option</th>
		    <th>Primary</th>
		    <th>Secondary</th>
		    <th>Time</th>
		</tr>
		<tr style="border: 1px solid black;">
		    <td>Initial Learning Assessment</td>
		    <td>FREE</td>
		    <td>FREE</td>
		    <td>Online</td>
		</tr>
		<tr style="border: 1px solid black;">
		    <td>Cognative/Learning Assessment</td>
		    <td>$100</td>
		    <td>$100</td>
		    <td>75 minutes</td>
		</tr>
		<tr style="border: 1px solid black;">
		    <td>Private Tutoring (1:1)</td>
		    <td>$50</td>
		    <td>$80</td>
		    <td>60 minutes</td>
		</tr>
		<tr style="border: 1px solid black;">
		    <td>Small Group Tutoring (up to 5 students)</td>
		    <td>$25 per student</td>
		    <td>$30 per student</td>
		    <td>60 minutes</td>
		</tr>
	    </table>

            </div>
        </div>
    </section>

@endsection
