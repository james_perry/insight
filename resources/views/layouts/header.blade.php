<div id="nav-bar" class="visible-xs">
    <div class="mm-header"><a href="#menu"><i class="fa fa-bars" aria-hidden="true"></i></a></div>
    <nav id="menu">
        <ul>
            <li class="active"><a href="index.html">Home</a></li>
            <li><a href="/about">About Us</a>
                <ul>
                    <li><a href="#">Why Us</a></li>
                </ul>
            </li>
            <li><a href="/courses">Courses</a>
		<!--
                <ul>
                    <li><a href="#">English Tutoring </a></li>
                    <li><a href="#">Reading Program</a></li>
                    <li><a href="#">Learning/Cognitive Assessments</a></li>
                    <li><a href="#">Subject Specific Tutoring</a></li>
                </ul>
		-->
            </li>
            <li><a href="/questions">Questionnaire</a></li>
            <li><a href="/pricing">Pricing</a></li>
            <!--<li><a href="#">Blog</a></li>-->
            <li><a href="#">Contact Us</a></li>
        </ul>
    </nav>
</div>

<div class="header">
    <div class="top_loginbar">
        <div class="container">
            @if (Auth::guest())
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-offset-6 col-md-6 col-sm-offset-4 col-sm-8">
                        <div class="row">
                            <div class="col-sm-4">

                                <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                               
                            </div>
                            <div class="col-sm-4">

                                <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                               
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Sign in
                                </button>

                            </div>
                            <div class="col-sm-2">
                                <a href="#"class="btn btn-info btn-block" data-toggle="modal" data-target="#myModal">Register</a>
                            </div>
							 @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
								 @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                </div>
            </form>
            @else
			<ul class='pull-right logout'>
			<li>

                @if(\Illuminate\Support\Facades\Auth::user()->is_admin == 1)

                    <a href="/admin/dashboard" class="user"> {{ Auth::user()->name }} <img src="/assets/images/user_icon.png"></a>
                @else
                    <a href="#" class="user"> {{ Auth::user()->name }}</a>
                @endif
				</li>
			<li>
            <a href="{{ url('/logout') }}" class="btn btn-info btn-block" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out</a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
               
				</ul>
                @endif

        </div>
    </div>
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <a href="/"><img src="images/logo.png" alt=""></a>
                </div>
                <div class="col-sm-9">
                    <div class="top_info">
                        <ul>
                            <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> info@insighteducation.com</a></li>
                            <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div id="nav" class="visible-lg visible-md visible-sm">
                        <ul>
                            <li><a href="/" class="active">Home</a></li>
                            <li><a href="/about">About Us</a>

                            </li>
                            <li><a href="/courses">Courses</a>
				<!--
                                <ul>
                                    <li><a href="#">English Tutoring </a></li>
                                    <li><a href="#">Reading Program</a></li>
                                    <li><a href="#">Learning/Cognitive Assessments</a></li>
                                    <li><a href="#">Subject Specific Tutoring</a></li>
                                </ul>
				-->
                            </li>

                                <li><a href="/questions">Free Assessment</a></li>

                            <li><a href="/pricing">Pricing</a></li>
                            <!--<li><a href="#">Blog</a></li>-->
                            <li><a href="#contactus">Contact Us</a></li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>
