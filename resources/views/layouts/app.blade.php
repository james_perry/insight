<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Design and Code by ProximateSolutions.com">
    <title>@yield('title')</title>
    
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!--custom css-->
    <link href="style.css" rel="stylesheet">
    <link href="{{ asset('/css/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/owl-carousel/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/responsive_menu.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('/css/responsive.css') }}" rel="stylesheet" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        $(function() {
            $('a[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 2000);
                        return false;
                    }
                }
            });
        });
    </script>

</head>
<body>

@include('layouts.header')
@yield('modal')
@yield('content')
@include('layouts.footer')

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.mmenu.min.all.js') }}"></script>
@yield('scriptfooter')

@if(count($errors) > 0)
    <script type="text/javascript">
        $(window).load(function(){
            $('#myModal').modal('show');
        });
    </script>
    @endif

<script type="text/javascript">
    $(function() {
        $('nav#menu').mmenu({
            extensions	: [ 'effect-slide-menu', 'pageshadow' ],
            searchfield	: true,
            counters	: true,
            navbar 		: {
                title		: ' '
            },
            navbars		: [
                {
                    position	: 'top',
                    content		: [ 'searchfield' ]
                }, {
                    position	: 'top',
                    content		: [
                        'prev',
                        'title',
                        'close'
                    ]
                },
            ]
        });
    });
</script>
</body>
</html>