<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Register</h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}


                    <div class="{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <label for="email" class="cols-sm-2 control-label">Enter your First Name</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                           <input id="first_name" type="text" placeholder="Enter your First Name" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
                            </div>
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <label for="email" class="cols-sm-2 control-label">Enter your Last Name</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input id="last_name" type="text" placeholder="Enter your Last Name" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>
                            </div>
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="{{ $errors->has('mobile') ? ' has-error' : '' }}">
                        <label for="email" class="cols-sm-2 control-label">Enter your Contact Number</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone fa" aria-hidden="true"></i></span>
                                <input id="mobile" type="text" placeholder="Enter your Contact Number" class="form-control" name="mobile" value="{{ old('mobile') }}" required autofocus>
                            </div>
                            @if ($errors->has('mobile'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="cols-sm-2 control-label">E-Mail Address</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                <input id="email" type="email" class="form-control"  placeholder="Enter your Email" name="email" value="{{ old('email') }}" required>
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="cols-sm-2 control-label">Password</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                       <input id="password" type="password" placeholder="Enter your Password" class="form-control" name="password" required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="">
                        <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                              <input id="password-confirm" type="password" placeholder="Confirm your Password" class="form-control" name="password_confirmation" required>

                            </div>
                        </div>
                    </div>


                    <div class=""><br>
                        <button type="submit" class="btn btn-primary btn-lg btn-block login-button">
                            Register
                        </button>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- End -->

<section class="contact_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8" id="contactus">

                <h2><strong>Contact</strong> Us</h2>
         <form class="form-horizontal" role="form" method="POST"  action="{{ url('/contactus') }}">
                        {{ csrf_field() }}
                        @include('_contact')
                    </form>

            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="white_box">
                    <div class="contact_info_box">
                        <ul class="contact_list">
                            <li><i class="fa fa-map-marker"></i>
                                <strong>Address:</strong><br>
                                address will goes here
                                address will goes here
                            </li>
                            <li><i class="fa fa-phone"></i>
                                <strong>Telephone:</strong><br>
                                <a href="tel:0123456789">0123456789</a>
                            </li>
                            <li><i class="fa fa-envelope-o"></i>
                                <strong>Email&nbsp;us:</strong><br>
                                <a href="mailto:info@insighteducation.com">info@InsightEducation.com</a>
                            </li>
                        </ul>

                    </div>
                    <div class="bottom_contact">
                        <ul class="cont_social">
                            <li><a title="Twitter" href="#" data-toggle="tooltip" data-placement="top"><i class="fa fa-twitter"></i></a></li>
                            <li><a title="Facebook" href="#" data-toggle="tooltip" data-placement="top"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a title="Google" href="#" data-toggle="tooltip" data-placement="top"><i class="fa fa-instagram"></i></a></li>
                            <li><a title="Pinterest" href="#" data-toggle="tooltip" data-placement="top"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-7">
                <div class="white_box">
                    <div class="map-wrapper">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29614829.448230013!2d115.21348248539898!3d-25.03096918791924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2b2bfd076787c5df%3A0x538267a1955b1352!2sAustralia!5e0!3m2!1sen!2s!4v1474377975328" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>


        </div>


    </div>

    </div>
    <div class="copyrights_bar">
        <div class="container text-center">
            <p>Copyrights 2016 InsightEducation. All Rights Reserved.</p>
            <p><a href="#">Terms and Conditions</a>  |  <a href="#">Privacy Policy</a></p>
        </div>
    </div>
</footer>


<div class="modal fade" id="Login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog login_box" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Login here</h3>
            </div>
            <div class="modal-body">
                <form class="comment-form">
                    <div class="row">
                        <div class="col-sm-4">
                            <input type="email" required class="form-control" placeholder="Email *">
                        </div>

                        <div class="col-sm-4">
                            <input type="password" class="form-control" placeholder="password">
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-user"></i> Login</button>
                        </div>
                    </div>

                </form>
                <div class="modal-footer">
                    <a href="#">Forgot your password?</a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog login_box" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Register</h3>
            </div>
            <div class="modal-body">
                <form class="comment-form">
                    <input required class="form-control" placeholder="Name *">
                    <input required class="form-control" placeholder="Email *">
                    <input required class="form-control" placeholder="Phone *">
                    <input required class="form-control" placeholder="password *">
                    <input type="checkbox" name="checkbox" id="checkbox">
                    <label for="checkbox">I accept the term of use</label>
                    <button class="btn btn-primary btn-block" type="submit">Join now!</button>
                </form>


            </div>
        </div>
    </div>
</div>