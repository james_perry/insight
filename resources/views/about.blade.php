
@extends('layouts.app')
@section('modal')<div class="container">

  
</div>

</body>

@endsection
@section('title')
    Insight Education
    @endsection
@section('content')

    <div class="banner">
        <div id="hero_banner" class="owl-carousel">
            <div class="item">
                <div class="container">
                    <div class="banner_caption">
			<h1>ABOUT US</h1>
                        @if (Auth::guest())
                            <a href="#" class="book_a_free_trial_button " data-toggle="modal" data-target="#myModal">Free Learning Assessment</a>

                            @else
                            <a href="#contactus" style="right: 300px;" class="book_a_free_trial_button" >Free Learning Assessment</a>
                        @endif


                    </div>
                </div>
                <img src="images/need_tutor.jpg" alt="">
            </div>


        </div>
    </div>
    <section id="welcome_section">
        <div class="container">
            <div class="col-sm-8 col-sm-offset-2">

            <h2><strong>Our Approach</strong></h2>
	    <ul>
		<li>Ideate Education offers specialised tutoring which focuses on teaching to student strengths and interests</li>
		<li>1:1 and small group tutoring available</li>
		<li>Our tutors create tailored learning profiles for your student</li>
		<li>We understand that multiple intelligences impact your child’s learning style and their ability to absorb information. Teaching a student how to learn and process information sets them up for success in their schooling, regardless of the subject or instruction</li>
		<li>Ideate creates open communication between our tutors and your child’s Teacher/School; ensuring that your student is best supported through their academic journey</li>
		<li>Subject specific tutoring in English, Maths, History, Geography, Society & Culture, Maths and Science available</li>
		<li>We offer a specialised, research-based Reading Program that is measurable and guaranteed to show student improvement and progress.</li>
	    </ul>

            </div>
        </div>
    </section>

    <section id="testimonials_section">
        <div class="container">
            <h2>Our Team</h2>
	<p>Ideate educators have specialised training in <i>MultiLit, Dibels, Reading to Learn</i> and <i>Reading Rockets</i>. Based on continued research through Sydney University with a focus on Learning difficulties, our founder, Tamarin Wood has developed a tailor made program that has improved the reading rate, fluency and comprehension levels of her students.</p>

	<p>With over 10 years experience teaching in Queensland and New South Wales, Tamarin has become the coordinator of a Learning Centre which focuses on students with specific learning difficulties and disabilities. Now working in the Eastern Suburbs of Sydney, she understands the need for early intervention for students who are struggling with literacy. Too often students arrive in High School without the ability to read and process information. This affects the student's capacity to engage in class and focus in most subject areas</p> 
        </div>
    </section>

    <section id="welcome_section">
        <div class="container">
            <div class="col-sm-8 col-sm-offset-2">

		    <h2><strong>Why Us?</strong></h2>
		    <p>Starting with an initial assessment, our tutors are able to grasp whether further testing is required for reading, spelling and/or numeracy. Our tailormade phonemic awareness tests allow our tutors to:</p>
		    <ul>
			<li>Predict the possibility of future reading difficulties in young children</li>
			<li>Measure fluency and accuracy of Primary and Secondary students</li>
			<li>Measure the comprehension level of Primary and Secondary students</li>
			<li>Provide a program that is student focused and allows the student to improve in these areas of reading and learning.</li>
		    </ul>
		    <p>YARK reading assessments, PATMaths and Cognitive Ability Tests are also available on request.</p>
		    <p>With an understanding of how your student learns, a program can be created to better support them on their journey to academic success.</p>
		    <p>Links to above programs and assessments:</p>
		    <ul>
			<li><a href="https://dibels.uoregon.edu/" target="_blank">Dibels program</a></li>
			<li><a href="http://www.multilit.com/" target="_blank">MultiLit program</a></li>
			<li><a href="https://www.acer.edu.au/pat/tests/mathematics" target="_blank">PATMaths</a></li>
			<li><a href="http://www.yarcsupport.co.uk/research.html" target="_blank">YARK reading</a></li>
			<li><a href="https://www.gl-assessment.co.uk/" target="_blank">Cognative Ability</a></li>
			<li><a href="https://www.readingtolearn.com.au/" target="_blank">Reading to Learn</a></li>
			<li><a href=" http://www.readingrockets.org/" target="_blank">Reading Rockets</a></li>
		    </ul>
		    <p>Our tutors have had training and experience with the above programs. Ideate’s Reading Program was written by our founder and is based on her interaction with some of the programs listed as well as her continued study at Sydney University and Charles Sturt University.</p>

            </div>
        </div>
    </section>

@endsection
