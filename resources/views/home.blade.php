
@extends('layouts.app')
@section('modal')<div class="container">

  
</div>

</body>

@endsection
@section('title')
    Insight Education
    @endsection
@section('content')

    <div class="banner">
        <div id="hero_banner" class="owl-carousel">
            <div class="item">
                <div class="container">
                    <div class="banner_caption">
                        <h1>proven results - guaranteed improvement</h1>
                        <h3>Join many happy families</h3>
                        @if (Auth::guest())
                            <a href="#" class="book_a_free_trial_button " data-toggle="modal" data-target="#myModal">Free Learning Assessment</a>

                            @else
                            <a href="#contactus" class="book_a_free_trial_button" >Free Learning Assessment</a>
                        @endif


                    </div>
                </div>
                <img src="images/banner1.png" alt="">
            </div>


        </div>
    </div>
    <div class="bottom_loginbar">
        <div class="container">
            @if (Auth::guest())
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
                            <div class="row">
                                <div class="col-sm-4">

                                    <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                </div>
                                <div class="col-sm-4">

                                    <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>

                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        Sign in
                                    </button>

                                </div>
                                <div class="col-sm-2">
                                    <a href="#"class="btn btn-info btn-block" data-toggle="modal" data-target="#myModal">Register</a>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            @else

            @endif
        </div>
    </div>
    <section id="welcome_section">
        <div class="container">
            <div class="col-sm-8 col-sm-offset-2">

                <h2><strong>Welcome</strong> to Ideate Education</h2>
                <h5>SPECIALISED TUTORING IN SYDNEY</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et tellus ipsum. Etiam augue velit, pharetra non dignissim in, volutpat sed ipsum. Ut velit lacus, ultricies at velit nec, maximus mattis ligula. In feugiat eu lacus non fermentum. Phasellus consectetur metus ligula, eu imperdiet purus accumsan mattis. In volutpat sollicitudin hendrerit. </p>

            </div>
        </div>
    </section>


    <section id="services_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <a href="#" class="box_img"><img src="images/kindy.jpg" alt=""></a>
                    <h3><a href="#">Pre-Kindy, Kindergarten and Year 1</a></h3>
                    <p>Fostering a love of learning from a young age ensures that children are happy and excited about going to school. We deliver lessons that are fun and engaging so children don’t even realise how quickly they’re progressing.</p>
                </div>
                <div class="col-sm-4">
                    <a href="#" class="box_img"><img src="images/h_img2.png" alt=""></a>
                    <h3><a href="#">Primary Years</a></h3>
                    <p>These are some of the most important years of your child’s schooling. This is where all of the groundwork is done to ensure that more difficult information and concepts can be learnt in their secondary years. There is a clear through line and progression in education. For example, students that struggle to read will struggle to spell and then find structuring correct sentences, paragraphs and essays difficult.</p>
                </div>
                <div class="col-sm-4">
                    <a href="#" class="box_img"><img src="images/secondary.jpg" alt=""></a>
                    <h3><a href="#">Secondary Years</a></h3>
                    <p>Ideate offers complete literacy programs for secondary students who feel that they are struggling with the demands of High School. We also offer subject specific tutoring in: English, Maths, History, Society and Culture and Geography. Our tutors have had great success in helping students improve their extended writing skills through structure and essay development lessons.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="testimonials_section">
        <div class="container">
            <h2>Update</h2>
            <div id="testimonials_slider" class="owl-carousel">
                <div class="item">
                    <p><strong>New NSW Literacy and Numeracy standards MANDATED for all students graduating Year 12 in 2020. Students sitting their NAPLAN tests in Year 9 during 2017 will need to reach a minimum standard in the lead up to their HSC and senior schooling. Now is the time to improve their reading, writing and numeracy ability. Ideate Education can improve your child’s literacy and numeracy levels.</strong></p>

                    <div class="testimonials_text_title">Subtitle??</div>
                    <div class="banner_caption">
                        @if (Auth::guest())
                            <a href="#" class="book_a_free_trial_button " data-toggle="modal" data-target="#myModal">Free Learning Assessment</a>

                            @else
                            <a href="#contactus" class="book_a_free_trial_button" >Free Learning Assessment</a>
                        @endif
		    </div>
                </div>
		<!--
                <div class="item">
                    <p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong> Mauris placerat nunc et felis laoreet commodo. Morbi semper sagittis augue. Pellentesque odio ex, molestie eu velit id, luctus pellentesque nulla. Nunc in hendrerit felis, et aliquet ligula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris rhoncus, eros a cursus congue, nibh risus iaculis velit, vestibulum suscipit sapien urna quis elit.</p>
                    <div class="testimonials_text_title">David Hilson</div>
                </div>
                <div class="item">
                    <p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong> Mauris placerat nunc et felis laoreet commodo. Morbi semper sagittis augue. Pellentesque odio ex, molestie eu velit id, luctus pellentesque nulla. Nunc in hendrerit felis, et aliquet ligula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris rhoncus, eros a cursus congue, nibh risus iaculis velit, vestibulum suscipit sapien urna quis elit.</p>
                    <div class="testimonials_text_title">David Hilson</div>
                </div>
		-->
            </div>
            <h5><a href="http://www.boardofstudies.nsw.edu.au/policy-research/stronger-hsc-standards/minimum-standard.html" target="_blank">Learn More</h5>
        </div>
    </section>

@endsection
