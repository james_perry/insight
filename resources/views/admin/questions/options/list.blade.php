@extends('admin.layouts.app')
@section('css')
    @endsection
@section('scripts')
    @endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Manage Answer
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="/admin/question"> Questions</a></li>
                <li class="active">options</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Options for question id - {{$id}}</h3>
                            <div class="pull-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newModal">
                                    Add New
                                </button>
                            </div>

                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Answer</th>
                                    <th>prefrence</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                @foreach($options as $option)
                                <tr>
                                    <td>{{$option->id}}</td>
                                    <td>{{$option->answer}}</td>
                                    <td>{{$option->position}}</td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal-{{$option->id}}">
                                            Edit
                                        </button></td>
                                    <td><a href="/admin/options/{{$option->id}}" class="btn btn-primary" onclick="event.preventDefault(); document.getElementById('option-{{$option->id}}-form').submit();">
                                            Delete
                                        </a>
                                        <form id="option-{{$option->id}}-form" action="/admin/options/{{$option->id}}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                            <input type="hidden" name="question" value="{{$id}}">

                                        </form>
                                    </td>

                                </tr>
                                    <div class="modal fade" id="editModal-{{$option->id}}" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Edit Option</h4>
                                                </div>

                                                <div class="modal-body">
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/options/{{$option->id}}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('patch') }}
                                                        <input type="hidden" name="question" value="{{$id}}">
                                                        <div class="{{ $errors->has('answer') ? ' has-error' : '' }}">
                                                            <label for="answer" class="cols-sm-2 control-label">Enter answer</label>
                                                            <div class="cols-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-question fa" aria-hidden="true"></i></span>
                                                                    <input id="answer" type="text"  class="form-control" value="{{$option->answer}}" name="answer" required autofocus>
                                                                </div>
                                                                @if ($errors->has('answer'))
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('answer') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>


                                                        <div class="{{ $errors->has('prefrence') ? ' has-error' : '' }}">
                                                            <label for="prefrence" class="cols-sm-2 control-label">Enter prefrence (only number allowed)</label>
                                                            <div class="cols-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-anchor fa" aria-hidden="true"></i></span>
                                                                    <input id="prefrence" type="text" placeholder="Enter question prefrence" class="form-control" name="prefrence" value="{{ $option->position or '' }}" required autofocus>
                                                                </div>
                                                                @if ($errors->has('prefrence'))
                                                                    <span class="help-block">
                                                                    <strong>{{ $errors->first('prefrence') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class=""><br>
                                                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                @endforeach

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                </div>
            </div>

        </section>
        <div class="modal fade" id="newModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create New Option</h4>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" role="form" method="POST" action="/admin/options">
                            {{ csrf_field() }}
                            <input type="hidden" name="question" value="{{$id}}">
                            <div class="{{ $errors->has('answer') ? ' has-error' : '' }}">
                                <label for="answer" class="cols-sm-2 control-label">Enter answer</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-question fa" aria-hidden="true"></i></span>
                                        <input id="answer" type="text"  class="form-control" value="" name="answer" required autofocus>
                                    </div>
                                    @if ($errors->has('answer'))
                                        <span class="help-block">
                                                                        <strong>{{ $errors->first('answer') }}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="{{ $errors->has('prefrence') ? ' has-error' : '' }}">
                                <label for="prefrence" class="cols-sm-2 control-label">Enter prefrence (only number allowed)</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-anchor fa" aria-hidden="true"></i></span>
                                        <input id="prefrence" type="text" placeholder="Enter question prefrence" class="form-control" name="prefrence" value="" required autofocus>
                                    </div>
                                    @if ($errors->has('prefrence'))
                                        <span class="help-block">
                                                                    <strong>{{ $errors->first('prefrence') }}</strong>
                                                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class=""><br>
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-button">
                                    Save
                                </button>
                            </div>
                        </form>

                    </div>

                </div>

            </div>
        </div>

    </div>
    @endsection