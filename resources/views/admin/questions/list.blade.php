@extends('admin.layouts.app')
@section('css')
    @endsection
@section('scripts')
    @endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Manage Questions
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Questions</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Questions List</h3>
                            <div class="pull-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newQuestionModal">
                                    Add New
                                </button>
                            </div>

                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Question</th>
                                    <th>Type</th>
                                    <th>prefrence</th>
                                    <th>Options</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                @foreach($questions as $question)
                                <tr>
                                    <td>{{$question->id}}</td>
                                    <td>{{$question->question}}</td>
                                    <td>@if($question->type == 1)Multiple Choice @else Single Option @endif</td>
                                    <td>{{$question->prefrence}}</td>
                                    <td><a href="/admin/options/{{$question->id}}" class="btn btn-primary">
                                            Options
                                        </a></td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal-{{$question->id}}">
                                            Edit
                                        </button></td>
                                    <td><a href="/admin/question/{{$question->id}}" class="btn btn-primary" onclick="event.preventDefault(); document.getElementById('question-{{$question->id}}-form').submit();">
                                            Delete
                                        </a>
                                        <form id="question-{{$question->id}}-form" action="/admin/question/{{$question->id}}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                        </form>
                                    </td>

                                </tr>
                                    <div class="modal fade" id="editModal-{{$question->id}}" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Edit Question</h4>
                                                </div>

                                                <div class="modal-body">
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/question/{{$question->id}}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('patch') }}

                                                        <div class="{{ $errors->has('question') ? ' has-error' : '' }}">
                                                            <label for="question" class="cols-sm-2 control-label">Enter Question</label>
                                                            <div class="cols-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-question fa" aria-hidden="true"></i></span>
                                                                    <textarea id="question" type="text"  class="form-control" name="question" required autofocus>{{$question->question}}</textarea>
                                                                </div>
                                                                @if ($errors->has('question'))
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('question') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>


                                                        <div class="{{ $errors->has('type') ? ' has-error' : '' }}">
                                                            <label for="type" class="cols-sm-2 control-label">Select Type</label>
                                                            <div class="cols-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-hand-o-right fa" aria-hidden="true"></i></span>
                                                                    <select id="type" class="form-control" name="type" required autofocus>
                                                                        <option @if($question->type == 1) selected="selected" @endif value="1">Multiple Choice</option>
                                                                        <option @if($question->type == 2) selected="selected" @endif value="2">Single Choice</option>
                                                                    </select>
                                                                </div>
                                                                @if ($errors->has('type'))
                                                                    <span class="help-block">
                                                                    <strong>{{ $errors->first('type') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="{{ $errors->has('prefrence') ? ' has-error' : '' }}">
                                                            <label for="prefrence" class="cols-sm-2 control-label">Enter prefrence (only number allowed)</label>
                                                            <div class="cols-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-anchor fa" aria-hidden="true"></i></span>
                                                                    <input id="prefrence" type="text" placeholder="Enter question prefrence" class="form-control" name="prefrence" value="{{ $question->prefrence or '' }}" required autofocus>
                                                                </div>
                                                                @if ($errors->has('prefrence'))
                                                                    <span class="help-block">
                                                                    <strong>{{ $errors->first('prefrence') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class=""><br>
                                                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                @endforeach

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                </div>
            </div>
            {{ $questions->links() }}
        </section>
        <div class="modal fade" id="newQuestionModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Question</h4>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/question') }}">
                            {{ csrf_field() }}


                            <div class="{{ $errors->has('question') ? ' has-error' : '' }}">
                                <label for="question" class="cols-sm-2 control-label">Enter Question</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-question fa" aria-hidden="true"></i></span>
                                        <textarea id="question" type="text"  class="form-control" name="question" required autofocus></textarea>
                                    </div>
                                    @if ($errors->has('question'))
                                        <span class="help-block">
                                                                        <strong>{{ $errors->first('question') }}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label for="type" class="cols-sm-2 control-label">Select Type</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-hand-o-right fa" aria-hidden="true"></i></span>
                                        <select id="type" class="form-control" name="type" required autofocus>
                                            <option value="1">Multiple Choice</option>
                                            <option value="2">Single Choice</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                                                    <strong>{{ $errors->first('type') }}</strong>
                                                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="{{ $errors->has('prefrence') ? ' has-error' : '' }}">
                                <label for="prefrence" class="cols-sm-2 control-label">Enter prefrence (only number allowed)</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-anchor fa" aria-hidden="true"></i></span>
                                        <input id="prefrence" type="text" placeholder="Enter question prefrence" class="form-control" name="prefrence" value="" required autofocus>
                                    </div>
                                    @if ($errors->has('prefrence'))
                                        <span class="help-block">
                                                                    <strong>{{ $errors->first('prefrence') }}</strong>
                                                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class=""><br>
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-button">
                                    Save
                                </button>
                            </div>
                        </form>

                    </div>

                </div>

            </div>
        </div>

    </div>
    @endsection