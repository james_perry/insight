@extends('admin.layouts.app')
@section('css')
    <style>
        .span12{
            border:solid 2px black;
            background-color:white;
        }
    </style>
@endsection
@section('scripts')
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Contact Form Details
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Forms Detail</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">User Details</h3>

                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Option</th>
                                        <th>Details</th>
                                    </tr>
                                        <tr>
                                            <td width="25">User Name</td>
                                            <td width="75">{{$question->firstname}} {{$question->lastname}}</td>
                                        </tr>
                                    <tr>
                                        <td width="25">Email</td>
                                        <td width="75">{{$question->email}}</td>
                                    </tr>

                                    <tr>
                                        <td width="25">Contact</td>
                                        <td width="75">{{$question->phone}}</td>
                                    </tr>

                                    <tr>
                                        <td width="25">Message</td>
                                        <td width="75">{{$question->message}}</td>
                                    </tr>
                                    <tr>
                                        <td width="25">Submission Date</td>
                                        <td width="75">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $question->created_at)->toFormattedDateString()}}</td>
                                    </tr>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>

        </section>

    </div>


@endsection