<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->first_name }} {{Auth::user()->last_name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="{{isactivepath("admin/dashboard")}}">
                <a href="/admin/dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="{{isActivePath('admin/question')}}">
                <a href="/admin/question">
                    <i class="fa fa-files-o"></i>
                    <span>Questions</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right">{{ \App\Question::count() }}</span>
            </span>
                </a>
            </li>
            <li class="{{isActivePath('admin/user')}}">
                <a href="/admin/user">
                    <i class="fa fa-user fa"></i> <span>Users</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="treeview {{areActivePaths(array('admin/newform', 'admin/form'))}}">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Forms Submitted</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{isActivePath('admin/newform')}}">
                        <a href="/admin/newform">
                            <i class="fa fa-circle-o"></i>
                            <span>New Forms</span>
                            <span class="pull-right-container">
                        <small class="label label-primary pull-right">{{ \App\Questionnaire::where('status', '1')->count() }}</small>
                            </span>
                        </a>
                    </li>
                    <li class="{{isActivePath('admin/form')}}">
                        <a href="/admin/form"><i class="fa fa-circle-o"></i>
                            <span> All Forms</span>
                            <span class="pull-right-container">
                        <small class="label label-primary pull-right">{{ \App\Questionnaire::count() }}</small>
                            </span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview {{areActivePaths(array('admin/contactus', 'admin/contactall'))}}">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Contact Us</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{isActivePath('admin/contactus')}}">
                        <a href="/admin/contactus">
                            <i class="fa fa-circle-o"></i>
                            <span>New</span>
                            <span class="pull-right-container">
                        <small class="label label-primary pull-right">{{ \App\ContactForm::where('status', '1')->count() }}</small>
                            </span>
                        </a>
                    </li>
                    <li class="{{isActivePath('admin/contactall')}}">
                        <a href="/admin/contactall"><i class="fa fa-circle-o"></i>
                            <span> All</span>
                            <span class="pull-right-container">
                        <small class="label label-primary pull-right">{{ \App\ContactForm::count() }}</small>
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>