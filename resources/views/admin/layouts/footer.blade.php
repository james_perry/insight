<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date("Y") ?> Insight Education.</strong> All rights
    reserved.
</footer>