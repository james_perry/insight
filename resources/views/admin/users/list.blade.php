@extends('admin.layouts.app')
@section('css')
    @endsection
@section('scripts')
    @endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Manage Questions
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Questions</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Questions List</h3>

                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Created on</th>
                                    <th>Questionnaires</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->first_name}} {{$user->last_name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->mobile}}</td>
                                    <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toFormattedDateString()}}</td>
                                    <td><a href="/admin/user/questionnaire/{{$user->id}}" class="btn btn-primary">
                                            Qustionnaire
                                        </a></td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal-{{$user->id}}">
                                            Edit
                                        </button></td>
                                    <td><a href="/admin/user/{{$user->id}}" class="btn btn-primary" onclick="event.preventDefault(); document.getElementById('user-{{$user->id}}-form').submit();">
                                            Delete
                                        </a>
                                        <form id="user-{{$user->id}}-form" action="/admin/user/{{$user->id}}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                        </form>
                                    </td>

                                </tr>
                                    <div class="modal fade" id="editModal-{{$user->id}}" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Edit User</h4>
                                                </div>

                                                <div class="modal-body">
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/user/{{$user->id}}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('patch') }}

                                                        <div class="{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                                            <label for="email" class="cols-sm-2 control-label">Enter your First Name</label>
                                                            <div class="cols-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                                                    <input id="first_name" type="text" placeholder="Enter your First Name" class="form-control" name="first_name" value="{{ $user->first_name }}" required autofocus>
                                                                </div>
                                                                @if ($errors->has('first_name'))
                                                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>


                                                        <div class="{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                                            <label for="email" class="cols-sm-2 control-label">Enter your Last Name</label>
                                                            <div class="cols-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                                                    <input id="last_name" type="text" placeholder="Enter your Last Name" class="form-control" name="last_name" value="{{ $user->last_name }}" required autofocus>
                                                                </div>
                                                                @if ($errors->has('last_name'))
                                                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                                            <label for="email" class="cols-sm-2 control-label">Enter your Contact Number</label>
                                                            <div class="cols-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-phone fa" aria-hidden="true"></i></span>
                                                                    <input id="mobile" type="text" placeholder="Enter your Contact Number" class="form-control" name="mobile" value="{{ $user->mobile }}" required autofocus>
                                                                </div>
                                                                @if ($errors->has('mobile'))
                                                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                                            <label for="email" class="cols-sm-2 control-label">E-Mail Address</label>
                                                            <div class="cols-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                                    <input id="email" type="email" class="form-control"  placeholder="Enter your Email" name="email" value="{{ $user->email }}" required>
                                                                </div>
                                                                @if ($errors->has('email'))
                                                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class=""><br>
                                                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                @endforeach

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                </div>
            </div>
        </section>
        <div class="modal fade" id="newQuestionModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Question</h4>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/question') }}">
                            {{ csrf_field() }}


                            <div class="{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="email" class="cols-sm-2 control-label">Enter your First Name</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input id="first_name" type="text" placeholder="Enter your First Name" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
                                    </div>
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="email" class="cols-sm-2 control-label">Enter your Last Name</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input id="last_name" type="text" placeholder="Enter your Last Name" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>
                                    </div>
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                <label for="email" class="cols-sm-2 control-label">Enter your Contact Number</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-phone fa" aria-hidden="true"></i></span>
                                        <input id="mobile" type="text" placeholder="Enter your Contact Number" class="form-control" name="mobile" value="{{ old('mobile') }}" required autofocus>
                                    </div>
                                    @if ($errors->has('mobile'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="cols-sm-2 control-label">E-Mail Address</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                        <input id="email" type="email" class="form-control"  placeholder="Enter your Email" name="email" value="{{ old('email') }}" required>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class=""><br>
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-button">
                                    Save
                                </button>
                            </div>
                        </form>

                    </div>

                </div>

            </div>
        </div>

    </div>
    @endsection