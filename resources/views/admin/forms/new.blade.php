@extends('admin.layouts.app')
@section('css')
@endsection
@section('scripts')
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Manage Form Submissions
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Forms</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Submitted Forms</h3>

                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>View</th>
                                    </tr>
                                    @foreach($forms as $form)
                                        <tr>
                                            <td>{{$form->id}}</td>
                                            <td>{{$form->user->first_name}} {{$form->user->last_name}}</td>
                                            <td>{{$form->user->email}}</td>
                                            <td>{{$form->user->mobile}}</td>
                                            <td><a href="/admin/form/{{$form->id}}" class="btn btn-primary">
                                                    View
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
            {{$forms->links()}}
        </section>
    </div>
@endsection