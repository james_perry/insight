@extends('admin.layouts.app')
@section('css')
    <style>
        .span12{
            border:solid 2px black;
            background-color:white;
        }
    </style>
@endsection
@section('scripts')
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Form Details
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Forms Detail</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">User Details</h3>

                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Option</th>
                                        <th>Details</th>
                                    </tr>
                                        <tr>
                                            <td width="25">User Name</td>
                                            <td width="75">{{$question->user->first_name}} {{$question->user->last_name}}</td>
                                        </tr>
                                    <tr>
                                        <td width="25">Email</td>
                                        <td width="75">{{$question->user->email}}</td>
                                    </tr>

                                    <tr>
                                        <td width="25">Contact</td>
                                        <td width="75">{{$question->user->mobile}}</td>
                                    </tr>

                                    <tr>
                                        <td width="25">Child Name</td>
                                        <td width="75">{{$question->name}}</td>
                                    </tr>
                                    <tr>
                                        <td width="25">Submission Date</td>
                                        <td width="75">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $question->created_at)->toFormattedDateString()}}</td>
                                    </tr>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Questions Details</h3>

                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Questions</th>
                                        <th>Answers</th>
                                    </tr>
                                    @foreach($quests as $key => $quest)
                                        <tr>
                                            <td>{{$key + 1}}</td>
                                            <td>{{ $quest->question }}</td>
                                            <td>
                                                @foreach($question->answers as $answer)
                                                    @if($answer->question_id == $quest->id)
                                                        {{$answer->option->answer}} <br>
                                                    @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Notes</h3>

                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Writer</th>
                                        <th>Date</th>
                                        <th>Notes</th>
                                    </tr>
                                    @foreach($question->notes as $key => $note)
                                        <tr>
                                            <td>{{$key + 1}}</td>
                                            <td>{{$note->user->first_name}} {{ $note->user->last_name}}</td>
                                            <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $note->created_at)->toFormattedDateString()}}</td>
                                            <td> {{$note->note}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 pull-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newQuestionModal">
                        Add Notes
                    </button>

                </div>
            </div>

        </section>
        <div class="modal fade" id="newQuestionModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Question</h4>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/addnote') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="question" value="{{$question->id}}">
                            <div class="{{ $errors->has('note') ? ' has-error' : '' }}">
                                <label for="note" class="cols-sm-2 control-label">Note</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-address-card fa" aria-hidden="true"></i></span>
                                        <textarea id="note" type="text"  class="form-control" name="note" required autofocus></textarea>
                                    </div>
                                    @if ($errors->has('note'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('note') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class=""><br>
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-button">
                                    Save
                                </button>
                            </div>
                        </form>

                    </div>

                </div>

            </div>
        </div>

    </div>


@endsection