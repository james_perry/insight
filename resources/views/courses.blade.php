
@extends('layouts.app')
@section('modal')<div class="container">

  
</div>

</body>

@endsection
@section('title')
    Insight Education
    @endsection
@section('content')

    <div class="banner">
        <div id="hero_banner" class="owl-carousel">
            <div class="item">
                <div class="container">
                    <div class="banner_caption">
			<h1>COURSES</h1>
                        @if (Auth::guest())
                            <a href="#" class="book_a_free_trial_button " data-toggle="modal" data-target="#myModal">Free Learning Assessment</a>

                        @else
                            <a href="#contactus" style="right: 300px;" class="book_a_free_trial_button" >Free Learning Assessment</a>
                        @endif
                    </div>
                </div>
                <img src="images/teacher.jpg" alt="">
            </div>


        </div>
    </div>
    <section id="welcome_section">
        <div class="container">
            <div class="col-sm-8 col-sm-offset-2">

            <h2><strong>What We Offer</strong></h2>
	    <h5>Kindergarten and Primary Tutoring</h5>
	    <ul>
		<li>1:1 and small group tutoring</li>
		<li>Phonemic awareness and literacy testing</li>
		<li>Individualised Learning Profiles created for students that can then be shared with teacher/school</li>
		<li>Targeted reading, spelling and writing program</li>
		<li>Handwriting improvement classes</li>
	    </ul>

	    <h5>Secondary</h5>
	    <ul>
		<li>1:1 and small group tutoring</li>
		<li>Cognitive, literacy and numeracy testing</li>
		<li>Individualised Learning Profiles created for students which can then be shared with teachers/school</li>
		<li>Targeted reading, spelling and writing program</li>
		<li>Handwriting improvement classes</li>
		<li><b>Subject specific tutoring in:</b> English, Maths, Science, History, Geography, Society and Culture</li>
	    </ul>

            </div>
        </div>
    </section>

    <!--
    <section id="testimonials_section">
        <div class="container"></div>
    </section>
    -->

@endsection
