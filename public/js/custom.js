$(document).ready(function() {
  
	  $("#hero_banner").owlCarousel({
        autoPlay: 10000,
        items : 1,
        pagination : false,
        singleItem : true,
        navigation : true,
        navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [979,1]
      });
	  
	  $("#blog_slider").owlCarousel({
        autoPlay: 12000,
        items :3,
        pagination : false,
        navigation : true,
        navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
		itemsMobile : [767,1],
        itemsDesktopSmall : [768,2],
        itemsDesktop : [1199,3]
      });
	  
	  $("#testimonials_slider").owlCarousel({  
        autoPlay: 10000,
        items : 1,
        pagination : true,
        singleItem : true,
        navigation : false,
        navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [979,1]
      });
});



// for tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

